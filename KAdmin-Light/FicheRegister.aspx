﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KAdmin-Light/MasterPage.master" AutoEventWireup="true" CodeFile="FicheRegister.aspx.cs" Inherits="FicheRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <!--debut js-->
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/jquery.js"></script>
    <script src="script/jquery-ui.js"></script>
     <script type="text/javascript">
         $(function () {
             //alert('ok');
            

             $('#btsubmit').click(function () {
                 var pres = {};
                 pres.nomprest = $.trim($('#idnom').val());
                 pres.prenprest = $.trim($('#idpren').val());
                 pres.societeprest = $.trim($('#idsoc').val());
                 pres.contactprest = $.trim($('#idtel').val());
                 pres.emailprest = $.trim($('#idmail').val());
                 pres.loginprest = $.trim($('#idlog').val());
                 pres.mdpprest = $.trim($('#idpass').val());
                 

                 var DTO = { 'pres': pres };

                 $.ajax({
                     type: "POST",
                     url: "FicheRegister.aspx/EnregistrerPres",
                     data: JSON.stringify(DTO),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (r) {
                         alert(r.d);

                     },
                     failure: function (r) {
                         alert(r.d);
                     }

                 });
                 return false;


                 
             });
         })
    </script>

    <!-- fin js-->

     <div class="row">
        <div class ="col-lg-12">
           
    <div class="col-lg-12">
        <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                                       
                                        
                                        <div class="panel panel-orange">
                                            <div class="panel-heading">
                                                Registration form</div>
                                            <div class="panel-body pan">
                                                <form action="#">
                                                <div class="form-body pal">
                                                                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idnom" type="text" placeholder="Nom" class="form-control" /></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idpren" type="text" placeholder="Prenom" class="form-control" /></div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idsoc" type="text" placeholder="Societe" class="form-control" /></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idtel" type="text" placeholder="Contact" class="form-control" /></div>
                                                        </div>
                                                        </div>
                                                    <hr />
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idlog" type="text" placeholder="Login" class="form-control" /></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input id="idpass" type="text" placeholder="Password" class="form-control" /></div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                     <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-envelope"></i>
                                                            <input id="idmail" type="text" placeholder="Email address" class="form-control" /></div>
                                                    </div>
                                                    <hr />
                                                    
                                                   
                                                    <div class="form-group mbn">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input tabindex="5" type="checkbox" />&nbsp; I agree with the Terms and Conditions</label></div>
                                                    </div>
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="button" id="btncancel" class="btn btn-primary">
                                                        Cancel</button>
                                                    <button type="button" id="btsubmit" class="btn btn-primary">
                                                        Submit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
            
        </div>


    </div>
</asp:Content>

