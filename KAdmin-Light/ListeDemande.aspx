﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KAdmin-Light/MasterPage.master" AutoEventWireup="true" CodeFile="ListeDemande.aspx.cs" Inherits="ListeDemande" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/jquery.js"></script>
    <script src="script/jquery-ui.js"></script>
    <script type="text/javascript"> 
       

        $(function () {
            //alert('ok');

            getListeDem();

            function getListeDem() {
                $.ajax({
                    type: "POST",
                    url: "ListeDemande.aspx/ListeDemandeP",
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                       // alert('ok');
                        var act = response.d;
                        $('#gv').empty();

                        $.each(act, function (index, lact) {
                            var time = lact.Dem.Datedebut.substring(6, 19);
                            var tim = lact.Dem.Datefin.substring(6, 19);
                            var dt = new Date(parseInt(time));
                            var d = new Date(parseInt(tim));

                            $('#gv').append('<tr><td>' + lact.Prest.Societeprest + '</td><td>' + lact.Prest.Nomprest + ' ' + lact.Prest.Prenprest + '</td><td>' + lact.Dem.Naturedem + '</td><td>' + lact.Dem.Criticite + '</td><td>' + dt.getDay() + '-' + dt.getMonth() + '-' + dt.getFullYear() + '<td>' + d.getDay() + '-' + d.getMonth() + '-' + d.getFullYear() + '</td></td><td><a class="btn" href="FicheValidation.aspx?natdem=' + lact.Dem.Naturedem + '&critic=' + lact.Dem.Criticite + '&codeus=' + lact.Dem.Codedem + '&dd=' + dt.getDay() + '-' + dt.getMonth() + '-' + dt.getFullYear() + '&df=' + d.getDay() + '-' + d.getMonth() + '-' + d.getFullYear() + '">action</a></td></tr>');
                            alert(lact.Dem.Codedem);
                        });
                        

                    }
                    ,
                    failure: function (msg) {
                        $('#gv').text(msg);
                        alert(msg);
                    }
                });
            }
            function redirectContrat(contrat, echeance, codeecheance) {
                //window.location.href = 'AjoutContrat.aspx?promoteur=' + nom + '&demande=' + code;
                function twRequeteVariable(sVariable) {
                    // Éliminer le "?"
                    var sReq = window.location.search.substring(1);
                    // Matrice de la requête
                    var aReq = sReq.split("&");
                    // Boucle les variables
                    for (var i = 0; i < aReq.length; i++) {
                        // Matrice de la variables / valeur
                        var aVar = aReq[i].split("=");
                        // Retourne la valeur si la variable 
                        // demandée = la variable de la boucle
                        if (aVar[0] == sVariable) { return aVar[1]; }
                    }
                    // Aucune variable de trouvée.
                    return (false);
                }

                alert(twRequeteVariable("natdem"));
                alert(twRequeteVariable("critic"));
                alert(twRequeteVariable("codeus"));
                alert(twRequeteVariable("dd"));
                alert(twRequeteVariable("df"));
                
            }

        })


    </script>
    <div class="row">
    <div class="col-lg-12">
        <div class="col-lg-12">
        <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
           
    
        
                        <div class="panel panel-yellow">
                            <div class="panel-heading">Simple Table</div>
                            <div class="panel-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Societe</th>
                                        <th>Nom</th>
                                        <th>Nature</th>
                                        <th>Criticite</th>
                                        <th>Datedebut</th>
                                        <th>Datefin</th>
                                    </tr>
                                    </thead>
                                    <tbody id="gv">
                                    <tr>
                                        <td>1</td>
                                        <td>Henry</td>
                                        <td>23</td>
                                        <td><span class="label label-sm label-success">Approved</span></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>John</td>
                                        <td>45</td>
                                        <td><span class="label label-sm label-info">Pending</span></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>30</td>
                                        <td><span class="label label-sm label-warning">Suspended</span></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Lahm</td>
                                        <td>15</td>
                                        <td><span class="label label-sm label-danger">Blocked</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
    
        </div>
         </div>
    </div>

    
</asp:Content>

