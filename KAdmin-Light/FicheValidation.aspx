﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KAdmin-Light/MasterPage.master" AutoEventWireup="true" CodeFile="FicheValidation.aspx.cs" Inherits="FicheValidation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <script src="script/jquery.js"></script>
      
    <div class="row">
        <div class ="col-lg-12">
           
    <div class="col-lg-12">
        <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                                       
                                        
                                        <div class="panel panel-grey">
                                            <div class="panel-heading">
                                                Fiche de Validation</div>
                                            <div class="panel-body pan">
                                                <form action="#" name="form2">
                                                <div class="form-body pal">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon" >
                                                                    <i class="fa fa-user"></i>
                                                                    <input id="nat" type="text" name="output" class="form-control" value="" /></div>
                                                            <!--<INPUT TYPE="text" NAME="output" VALUE=""> Zone de texte de sortie -->

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-user"></i>
                                                                    <input id="cri" type="text" placeholder="Criticité" class="form-control" /></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-envelope"></i>
                                                                    <input id="ddd" type="text" placeholder="Date debut" class="form-control" /></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-phone"></i>
                                                                    <input id="dff" type="text" placeholder="Date fin" class="form-control" /></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div class="row">
                                                        
                                                    <div class="form-group">
                                                        <div class="radio">
                                                            <label class="radio-inline">
                                                                <input id="Radio1" type="radio" name="optionsRadios" value="accordee" checked="checked" />&nbsp;
                                                                Validation

                                                            </label>
                                                            <label class="radio-inline">
                                                                    <input id="Radio2" type="radio"
                                                                    name="optionsRadios" value="rejette" />&nbsp; 
                                                                Refus

                                                            </label>

                                                        </div>
                                                    </div>
                                                    
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                        <textarea rows="5" id="motif" placeholder="Additional info" class="form-control"></textarea></div>
                                                    <hr />
                                                    
                                                    <div class="form-group">
                                                           
                                                    
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="button" id="btnSubmit" class="btn btn-primary">
                                                        Continue</button>
                                                    <button type="button" id="btnCancel" class="btn btn-primary">
                                                        Cancel</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
            
        </div>


    </div>

     <script type="text/javascript">
         function twRequeteVariable(sVariable) {
             // Éliminer le "?"
             var sReq = window.location.search.substring(1);
             // Matrice de la requête
             var aReq = sReq.split("&");
             // Boucle les variables
             for (var i = 0; i < aReq.length; i++) {
                 // Matrice de la variables / valeur
                 var aVar = aReq[i].split("=");
                 // Retourne la valeur si la variable 
                 // demandée = la variable de la boucle
                 if (aVar[0] == sVariable) { return aVar[1]; }
             }
             // Aucune variable de trouvée.
             return (false);
         }

         var demnat = twRequeteVariable('natdem');
         var crit = twRequeteVariable('critic');
         var ddb = twRequeteVariable('dd');
         var ddf = twRequeteVariable('df');
         var cdem = twRequeteVariable('codeus');
        

          var tr = document.getElementById('nat');
          var tc = document.getElementById('cri');
          var tdf = document.getElementById('dff');
          var tdd = document.getElementById('ddd');
        
         tr.value = demnat;
         tc.value = crit;
         tdd.value = ddb;
         tdf.value = ddf;
         
         //alert(cdem);
         $(function () {
             //alert('ok');
             $('#btnSubmit').click(function () {
                 var option = $('input[type=radio][name=optionsRadios]:checked').attr('value');
                 //var motif = $('#motif').val();
                 //rep.Validation = option;
                 //rep.Motifvalidation = $.trim($('#motif').val());
                 //rep.Refus = option;
                 //rep.Motifrefus = $('#motif').val();

                 
                 //alert(motif);
                 var rep = {};
                 //var cook = cookie.codeuser;
                 //alert(cook);
                 rep.Validation = option;//$.trim($('#').val());
                 rep.Motifvalidation = $.trim($('#motif').val());
                 rep.Refus = option;// $.trim($('#').val());
                 rep.Motifrefus = $('#motif').val();
                 rep.Dem.Codedem = cdem;// $.trim($('#').val());
                 rep.User.Codeuser = //$.trim($('#').val());
                 //alert(rep.User.Codeuser);

                
                 var DTO = { 'rep': rep };
                 $.ajax({
                     type: "POST",
                     url: "FicheValidation.aspx/ReponseInserer",
                     data: JSON.stringify(DTO),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (r) {
                         alert(r.d);
                         

                     },
                     failure: function (r) {
                         alert(r.d);
                     }

                 });
                 return false;


                 
             });
                 
             

         });
    </script>

</asp:Content>

