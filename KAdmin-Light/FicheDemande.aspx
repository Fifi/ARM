﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KAdmin-Light/MasterPage.master" AutoEventWireup="true" CodeFile="FicheDemande.aspx.cs" Inherits="FicheDemande" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <script type="text/javascript">
        

    </script>
    <div class="row">
        <div class ="col-lg-12">
             <div class="col-lg-12">

            <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>

            <div class="panel panel-grey">
                                            <div class="panel-heading">
                                                Checkout form</div>
                                            <div class="panel-body pan">
                                                <form action="#">
                                                <div class="form-body pal">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-user"></i>
                                                                    <input id="valnature" type="text" placeholder="Nature demande" class="form-control" /></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-user"></i>
                                                                    <input id="valtech" type="text" placeholder="Nom technicien" class="form-control" /></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                   
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                <select class="form-control" id="cbosite">
                                                                    <option>Site</option>
                                                                </select></div>
                                                                </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="valdesc" type="text" placeholder="Description" class="form-control" /></div>
                                                    <div class="form-group">
                                                        <textarea rows="5" placeholder="Observations" id="valobserv" class="form-control"></textarea></div>
                                                    <hr />
                                                    <div class="form-group">
                                                        <div class="radio">
                                                            <label class="radio-inline">
                                                                <input id="optionsVisa" type="radio" name="optionsRadios" value="Visa" checked="checked" />&nbsp;
                                                                Faible</label><label class="radio-inline"><input id="optionsMasterCard" type="radio"
                                                                    name="optionsRadios" value="MasterCard" />&nbsp; Moyen</label><label class="radio-inline"><input
                                                                        id="optionsPayPal" type="radio" name="optionsRadios" value="PayPal" />&nbsp; elevee</label>
                                                            <label class="radio-inline"><input id="Radio1" type="radio" name="optionsRadios" value="PayPal" />&nbsp; aucun</label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group mbn">
                                                                <label class="pts">
                                                                    Beginning date</label></div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <select class="form-control">
                                                                    <option>Month</option>
                                                                </select></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group mbn">
                                                                <input id="inputYear" type="text" placeholder="Year" class="form-control" /></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group mbn">
                                                                <label class="pts">
                                                                    Expiration date</label></div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <select class="form-control">
                                                                    <option>Month</option>
                                                                </select></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group mbn">
                                                                <input id="Text1" type="text" placeholder="Year" class="form-control" /></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-primary">
                                                        Continue</button>
                                                    <button type="submit" class="btn btn-primary">
                                                        Cancel</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
        </div>
            </div>
    </div>
</asp:Content>

