<?php

/** fonction de chargement automatique des classes
 * @param Strring $class nom de la classe a charger
 * @return null cette fonction ne retourne rien
 */
function chargement($class){

       $dir = __DIR__;
      
       
       if(is_file($dir.'/Managers/'.$class.'.php'))
       {
          
           $chaine = $dir.'/Managers/'.$class.'.php'; 
           
       }else if (is_file($dir.'/classes/'.$class.'.php')){
           
           $chaine = $dir.'/classes/'.$class.'.php';
           
       }
        // echo $chaine ;
        require $chaine;
}

spl_autoload_register('chargement');