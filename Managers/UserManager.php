<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserManager
 *
 * @author ACER
 */
class UserManager extends Manager {
    private $_db;
    
    function __construct(){
        $this->_db = parent::__construct();
    }
    
    public function authentifier($login, $pass) {
        $result =  Array();
            
        $sql = "CALL User_connexion(:login, :pass)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':login',$login);
        $requete->bindValue(':pass',$pass);
        $requete->execute();
        $requete->setFetchMode(PDO::FETCH_ASSOC);

        while( $ligne = $requete->fetch()) // on r�cup�re la liste 
        {

                $result[]=$ligne; //
        }
        return $result;
    }
     public function authentifierp($login, $pass, $profil) {
        $result =  Array();
            
        $sql = "CALL   sp_authentifier_user_profil(:login, :pass, :prof)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':login',$login);
        $requete->bindValue(':pass',$pass);
        $requete->bindValue(':prof',$profil);
        $requete->execute();
        $requete->setFetchMode(PDO::FETCH_ASSOC);

        while( $ligne = $requete->fetch()) // on r�cup�re la liste 
        {

                $result[]=$ligne; //
        }
        return $result;
    }
    
    public function ajouterUser($nomuse, $prenuse,$emailuse,$contactuse,$loginuse,$mdpuse,$codefon){
        $sql = "CALL sp_User_inserer(:nomuse, :prenuse,:emailuse,:contactuse,:loginuse,:mdpuse,:codefon)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':nomuse',$nomuse);
        $requete->bindValue(':prenuse',$prenuse);
        $requete->bindValue(':emailuse',$emailuse);
        $requete->bindValue(':contactuse',$contactuse);
        $requete->bindValue(':loginuse',$loginuse);
        $requete->bindValue(':mdpuse',$mdpuse);
        $requete->bindValue(':codefon',$codefon); 
                     
       try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            return $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
    //put your code here
    
    public function modifierUser($codeuse,$nomuse, $prenuse,$emailuse,$contactuse,$loginuse,$mdpuse,$codefon){
        $sql = "CALL sp_User_inserer(:codeuse, :nomuse, :prenuse,:emailuse,:contactuse,:loginuse,:mdpuse,:codefon)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':codeuse',$codeuse);
        $requete->bindValue(':nomuse',$nomuse);
        $requete->bindValue(':prenuse',$prenuse);
        $requete->bindValue(':emailuse',$emailuse);
        $requete->bindValue(':contactuse',$contactuse);
        $requete->bindValue(':loginuse',$loginuse);
        $requete->bindValue(':mdpuse',$mdpuse);
        $requete->bindValue(':codefon',$codefon); 
                     
       try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            return $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
    
}
