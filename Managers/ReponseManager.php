<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReponseManager
 *
 * @author ACER
 */
class ReponseManager extends Manager {
    private $_db;
    public function __construct() {
        $this->_db = parent::__construct();
    }
    
   public function ajouterReponse($option,$motiff,$codedemand,$codeuti){
        $sql = "CALL sp_reponses_inserer(:optione,:motif,:codedem,:codeuser)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':optione',$option);
        $requete->bindValue(':motif',$motiff);
        $requete->bindValue(':codedem',$codedemand);
        $requete->bindValue(':codeuser',$codeuti);
                     
        try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return $exc->getMessage();
            
        }
    }
   
    
    public function modifierReponse($coderep, $daterep, $validationrep,$motifval,$refusrep,$motifref,$codedem,$codeuser){
       $sql = "CALL sp_Reponse_modifier(:codere,:datere,:val,:motifv,:refusl,:motirefu,:codede,:codeuse)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':codere',$coderep);
        $requete->bindValue(':datere',$daterep);
        $requete->bindValue(':val',$validationrep);
        $requete->bindValue(':motifv',$motifval);
         $requete->bindValue(':refusl',$refusrep);   
          $requete->bindValue(':motifrefu',$motifref);
           $requete->bindValue(':codede',$codedem);
            $requete->bindValue(':codeuse',$codeuser);
             
                   
       try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            return $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
     public function listerreponse() {
        $result =  Array();
        $sql = "CALL sp_ListerReponse()";
        $requete=$this->_db->prepare($sql);
        $requete->execute();
            
        $requete->setFetchMode(PDO::FETCH_ASSOC);

        while( $ligne = $requete->fetch()) // on r�cup�re la liste 
        {

                $result[]=$ligne; //
        }
        return $result;
    }
   
    //put your code here
}
