<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DemandeManager
 *
 * @author ACER
 */
class DemandeManager extends Manager{
    
    private $_db;
    
    function __construct(){
        $this->_db = parent::__construct();
    }
     
     public function ajouterDemande($naturedem, $nomtech,$description,$observ,$crit,$dd,$df,$codesit,$contact1,$contact2,$contact3){
        $sql = "CALL sp_Demande_inserer(:naturedem, :nomtech,:description,:observ,:crit,:dd,:df,:codesit,:contact1,:contact2,:contact3)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':naturedem',$naturedem);
        $requete->bindValue(':nomtech',$nomtech);
        $requete->bindValue(':description',$description);
        $requete->bindValue(':observ',$observ);
        $requete->bindValue(':crit',$crit);
        $requete->bindValue(':dd',$dd);
        $requete->bindValue(':df',$df); 
        $requete->bindValue(':codesit',$codesit); 
        $requete->bindValue(':contact1',$contact1);
        $requete->bindValue(':contact2',$contact2);
        $requete->bindValue(':contact3',$contact3);     
       
  
                   
       try {
            $requete->execute();
            //alert("fait");
           
        }
        catch (Exception $exc) {
            
            echo $exc->getTraceAsString();
            return $exc->getMessage();
        }
    }
   
     public function modifierDemande($codedem, $naturedem, $nomtech,$description,$observ,$crit,$dd,$df,$codesit,$contact1,$contact2,$contact3){
        $sql = "CALL sp_Demande_inserer(:codedem, :naturedem, :nomtech,:description,:observ,:crit,:dd,:df,:codesit,:contact1,:contact2,:contact3)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':codedem',$codedem);
        $requete->bindValue(':naturedem',$naturedem);
        $requete->bindValue(':nomtech',$nomtech);
        $requete->bindValue(':description',$description);
        $requete->bindValue(':observ',$observ);
        $requete->bindValue(':crit',$crit);
        $requete->bindValue(':dd',$dd);
        $requete->bindValue(':df',$df); 
        $requete->bindValue(':codesit',$codesit); 
        $requete->bindValue(':contact1',$contact1);
        $requete->bindValue(':contact2',$contact2);
        $requete->bindValue(':contact3',$contact3); 
             
                         
       try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return $exc->getMessage();
            
        }
    }
   
    //put your code here
}
