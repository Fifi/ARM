<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manager
 *
 * @author ACER
 */
class Manager {
    public function __construct(){
	// connexion a une base de données MySQL
	$dsn = "mysql:host=127.0.0.1;dbname=arm";
        $login = "root";
	$pass = "";
	
	try {
            
            return new PDO($dsn, $login, $pass, array(PDO::ATTR_ERRMODE,  PDO::ERRMODE_WARNING));
            
	} catch (PDOException $e) 
        {
         //==== Si ya une erreur on l'affiche l'erreur 
         exit( $e->getMessage());
        }
	
    }
        
}
