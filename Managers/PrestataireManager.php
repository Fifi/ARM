<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrestataireManager
 *
 * @author ACER
 */
class PrestataireManager extends Manager {
    private $_db;
    public function __construct() {
        $this->_db = parent::__construct();
    }
    
     public function ajouterPrestataire($nomprest, $prenprest,$societeprest,$contactprest,$emailprest,$loginprest,$mdpprest){
        $sql = "CALL sp_Prestataire_inserer(:nomprest, :prenprest,:societeprest,:contactprest,:emailprest,:loginprest,:mdpprest)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':nomprest',$nomprest);
        $requete->bindValue(':prenprest',$prenprest);
        $requete->bindValue(':societeprest',$societeprest);
        $requete->bindValue(':contactprest',$contactprest);
        $requete->bindValue(':emailprest',$emailprest);
        $requete->bindValue(':loginprest',$loginprest);
        $requete->bindValue(':mdpprest',$mdpprest); 
                     
       try {
            $requete->execute();           
        }
        catch (Exception $exc) {
            return $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
   
     public function modifierPrestataire($codeprest,$nomprest, $prenprest,$societeprest,$contactprest,$emailprest,$loginprest,$mdpprest){
        $sql = "CALL sp_Prestataire_inserer(:codeprest,:nomprest, :prenprest,:societeprest,:contactprest,:emailprest,:loginprest,:mdpprest)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':codeprest',$codeprest);
        $requete->bindValue(':nomprest',$nomprest);
        $requete->bindValue(':prenprest',$prenprest);
        $requete->bindValue(':societeprest',$societeprest);
        $requete->bindValue(':contactprest',$contactprest);
        $requete->bindValue(':emailprest',$emailprest);
        $requete->bindValue(':loginprest',$loginprest);
        $requete->bindValue(':mdpprest',$mdpprest); 
                     
       try {
            $requete->execute();           
        }
        catch (Exception $exc) {
            return $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
    
    public function listerdemande() {
        $result =  Array();
        $sql = "CALL sp_ListerDemandePrest()";
        $requete=$this->_db->prepare($sql);
        $requete->execute();
            
        $requete->setFetchMode(PDO::FETCH_ASSOC);

        while( $ligne = $requete->fetch()) // on r�cup�re la liste 
        {

                $result[]=$ligne; //
        }
        return $result;
    }
    
    public function listerpresdem($pres) {
        $result =  Array();
        $sql = "CALL sp_ListerPresdem(:codepress)";
        $requete=$this->_db->prepare($sql);
        $requete->execute();
            
        $requete->setFetchMode(PDO::FETCH_ASSOC);

        while( $ligne = $requete->fetch()) // on r�cup�re la liste 
        {

                $result[]=$ligne; //
        }
        return $result;
    }
   
    //put your code here
}
