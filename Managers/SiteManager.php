<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteManager
 *
 * @author ACER
 */
class SiteManager extends Manager {
    
 
    private $_db;
    
    function __construct(){
        $this->_db = parent::__construct();
    }
    public function ajouterSite($nomsite,$localisationsite,$numerosite){
        $sql = "CALL sp_Site_inserer(:nomsit,:localisationsit,:numerosit)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':nomsit',$nomsite);
        $requete->bindValue(':localisationsit',$localisationsite);
        $requete->bindValue(':numerosit',$numerosite);
       
        try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return $exc->getMessage();
            
        }
           
        
    }
    
    public function modifierSite($codesite,$nomsite,$localisationsite,$numerosite){
        $sql = "CALL sp_Site_modifier(:codesit,:nomsit,:localisationsit,:numerosit)";
        $requete=$this->_db->prepare($sql);
        $requete->bindValue(':codesit',$codesite);      
        $requete->bindValue(':nomsit',$nomsite);
        $requete->bindValue(':localisationsit',$localisationsite);
        $requete->bindValue(':numerosit',$numerosite);
       try {
            $requete->execute();
           
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return $exc->getMessage();
            
        }
    }
    
        
    }
    //put your code here


    //put your code here

