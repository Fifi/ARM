/**
 * 
 */

var myApp = angular.module("IHSWeb",[]);

myApp .config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
                when ('/home',{templateUrl:'view/MasterPage.html', controller:'HomeController'}).
                when ('/listedemande',{templateUrl:'view/ListeDemande.html', controller:'DemandelisteController'}).
                when ('/listedemandepres',{templateUrl:'view/ListeDemandePres.html', controller:'DemandelistepresController'}).
                when ('/valide',{templateUrl:'view/FicheValidation.html', controller:'ValidationController'}).
                when ('/site',{templateUrl:'view/FicheSite.html', controller:'SiteController'}).
                when ('/demande',{templateUrl:'view/FicheDemande.html', controller:'RegisterdemandeController'}).
                when ('/register',{templateUrl:'view/FicheRegister.html', controller:'RegisterpresController'}).
                when ('/listereponse',{templateUrl:'view/ListeReponse.html', controller:'ValidationController'}).
                when ('/listepres',{templateUrl:'view/ListePresdem.html', controller:'PrestairelisteController'}).
                 
               when ('/loginuser',{templateUrl: 'view/PagedeConnexionUser.html', controller: 'ConnexionController'}).
                when ('/login',{templateUrl: 'view/PagedeConnexion.html',controller: 'ConnexionController'}).
                
                otherwise({redirectTo: '/home'});
    }]);




//myApp .config(['$routeProvider',
//    function($routeProvider){
//        $routeProvider.
//                when ('/Show',{templateUrl:'pages/Show.html', controller:'ShowController'}).
//                when ('/Show/:param',{templateUrl: 'pages/ShowParam.html?:param', controller:'ShowParamController'}).
//                when ('/Add',{
//                    templateUrl: 'pages/Add.html',
//                    controller: 'AddController'
//        }).
//                when ('/Local',{
//                    //templateUrl: 'pages/Local.html',
//                    templateUrl: 'pages/Add.html',
//                    controller: 'LocalTemplateController'
//        }).
//                when ('/Data',{
//                    templateUrl: 'pages/CommonData.html',
//                    controller: 'CommonDataController',
//                    foodata : 'Common Data'
//        }).
//                otherwise({redirectTo: '/Show'});
//    }]);
//
//myApp.controller('ShowController',function($scope){
//    $scope.message = 'This is the Show Controller';
//});
//
//myApp.controller('AddController',function($scope){
//    $scope.message = 'This is the Add Controller';
//});
//
//myApp.controller('ShowParamController',function($scope, $routeParams){
//    $scope.message = 'This is the ShowParam Controller with param = '/*+$routeParams.param*/;
//    $scope.param = $routeParams.param;
//    
//});
//
//myApp.controller('LocalTemplateController',function($scope){
//    $scope.message = 'This is the Local Temp Controller ';
//});
//
//myApp.controller('CommonDataController',function($scope, $route){
//    $scope.message = 'This is the Common Data Controller & Data = ';
//    $scope.data = $route.current.foodata;
//});

