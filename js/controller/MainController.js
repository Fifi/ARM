
function MainController($scope,$location){
    
    $scope.Utilisateur = {
        nom:"",
        prenom:"",
        fonction:""
    }
    $scope.header = function(){
        return $location.path() == '/Home';
    }
    $scope.connexionPath = function(){
        return $location.path() == '/login';
    }
    $scope.mainInit = function(){
        var value = $.cookie("ARM");
        if((typeof value == 'undefined') || (value == null)){
            location.href= location.protocol+'//'+location.host+'/ARM/view/Login.html';
        }else{
        value = JSON.parse($.cookie("ARM"))[0];
        console.info("ARM cookie", value);
        
        $scope.Utilisateur.nom = value.nomuser;
        $scope.Utilisateur.prenom = value.prenuser;
        $scope.Utilisateur.fonction = value.libfonctione;

            if((value.libellefonctione == 'prestataire')){
                if($location.path() != '/Home'){
                    $location.path('/listedemandepres'); 
                }
            }

        }
    }
}
